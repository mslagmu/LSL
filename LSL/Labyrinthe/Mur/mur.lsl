integer channel = 134;

default
{
    state_entry()
    {
        llListen(channel, "", "", "");
    }

    listen( integer canal, string name, key id, string message )
    {
        if ( message == "delete") llDie();
        if ( message == "50")  llSetAlpha(0.3, ALL_SIDES );
        if ( message == "0")  llSetAlpha(0, ALL_SIDES );
        if ( message == "100")  llSetAlpha(1.0, ALL_SIDES );
    }
}
