const express = require("express");
const app = express();

const laby = require("./laby.js")

app.use(express.static("public"));

// https://expressjs.com/en/starter/basic-routing.html
app.get("/", (request, response) => {
  response.send(laby.execute("text"))
});

// send the default array of dreams to the webpage
app.get("/svg", (request, response) => {
    response.setHeader('content-type', 'image/svg+xml');
    response.send(laby.execute("svg"))
});

const listener = app.listen(8000, () => {
    console.log("Your app is listening on port " + listener.address().port);
  })

  