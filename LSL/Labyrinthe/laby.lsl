integer canalRLV=-1812221819;
string url = "http://oslove.free.fr/maze/v1.php";
//string url = "http://88.168.224.22/laby.php";
float largeur = 3.0;

vector initdesign;

integer canal;
key http_request_id;
rotation rot90Z;
rotation rot0   = <0.0,0.0,0.0,1.0>;

integer nowall = 0;

vector initpos;

moveTo(vector v) {
    while (llGetPos() != v) {
        llSetPos(v);    
    }
    
}


makeTop(integer x, integer y) {
    
    float xpos = x * largeur ;
    float ypos = (-y-0.5)*largeur;
    
    
    vector pos = initdesign + <xpos,ypos,0.0>;
    
    moveTo(pos);
    
    
    llRezObject("Mur", pos, <0.0,0.0,0.0>, rot90Z , 0);
}


makeRight(integer x, integer y) {
    
    float xpos = (x + 0.5)  * largeur ;
    float ypos = -y  *largeur;
    
    vector pos = initdesign + <xpos,ypos,0.0>;
    
     moveTo(pos);
    
    
    llRezObject("Mur", pos, <0.0,0.0,0.0>, rot0 , 0);
}

build() {
          rot90Z = llEuler2Rot(<0.0,0,90> * DEG_TO_RAD );
      initpos = llGetPos();
      initdesign = initpos + <largeur / 2.0, -largeur / 2.0,0>;
      
      moveTo(initdesign);
      

      
      http_request_id = llHTTPRequest(url, [], "");   
}

building(string body) {
    
    // llParseString2List( string src, list séparateurs_supprimés, list séparateurs_conservés );
    list lines=llCSV2List(body);
    integer i;
    string line;
    list cmd;
    integer x;
    integer y;
    string sens;
    
    for ( i=0; i < llGetListLength(lines)-1; i++) {
        line = llList2String(lines,i);
        cmd = llParseString2List(line,[":"],[]);
        sens = llList2String(cmd,0);
        x = llList2Integer(cmd,1);
        y = llList2Integer(cmd,2);
        
        if ( sens == "top" ) {
            makeTop(x,y);
        } else {
             makeRight(x,y);    
        }
        
    }
    
    moveTo(initpos);
    
}


default
{
    
    state_entry()
    {   // Crée un canal au hasard dans l'intervalle [-1000000000,-2000000000]
    canal= (integer) (llFrand(-1000000000.0) - 1000000000.0);
 
    llListen(canal, "", "", "");
    llListen(-134, "", "", "");
    }
    
    touch_start(integer total_number)
    {
      if ( ! llDetectedGroup(0)  ) { return;} 
      if ( nowall == 1) {
        llDialog(llDetectedKey(0),"Que voulez vous faire ?",["Construire","Aide"],canal);
      } else {
        llDialog(llDetectedKey(0),"Que voulez vous faire ?",["Detruire","100","50","0","Aide"],canal);  
     }
         
    }
    
    listen(integer chan, string nom, key id, string message)
    {
        if (chan == canal ) {
            if ( message == "Construire" ) {
                llSay(0,"Debut de la construction");
                build();
                nowall = 0;
            }
            if ( message == "Detruire" ) {
                llRegionSay(134,"delete");
                nowall=1;
            }
            
            if ( message == "100" ) {
                llRegionSay(134,"100");
            }
            
            
            if ( message == "0" ) {
                llRegionSay(134,"0");
            }
            
            if ( message == "50" ) {
                llRegionSay(134,"50");
            }
            
            if (message == "Aide") {
                llDialog(id,"Aide",["Manuel","Php","Adresse"],canal);
            }
            
            if (message == "Manuel") {
                llSay(0,"Non Implementé");
            }
            
            if (message == "Php") {
                llGiveInventory(id, "laby.php");
            }
            
            if (message == "Adresse") {
                llGiveInventory(id, "laby.php");
            }
            
        }
        if (chan == -134 ) {
            llRegionSay(canalRLV,message);
        }
    }
    
    http_response(key request_id, integer status, list metadata, string body)
    {
        if (request_id == http_request_id)
        {
           building(body);
           llSay(0,"Fin de la construction");
        }
    }
    
}

