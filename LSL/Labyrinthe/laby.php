<?

function gettest($key,$value) {
    if (array_key_exists($key, $_GET)) {
        return $_GET[$key];
    } else {
        return $value;
    }
}


class svgMaze {
    
    var $larg = 25;
    
    function head(){
        echo '<?xml version="1.0" standalone="no"?>';
        echo '<!DOCTYPE svg SYSTEM "svg-19991203.dtd" >';
        echo '<svg>';
    }
    
    function line($x,$y,$sens) {
        if ($sens == 0) {
            $x1 = $x *$this->larg;
            $y1 = ($y+1)*$this->larg;
            $x2=$x1+$this->larg;
            $y2=$y1;
        } else {
            $x1 = ($x+1)*$this->larg;
            $y1 = $y*$this->larg;
            $x2=$x1;
            $y2=$y1+$this->larg;
        }
        ?>
        <line  x1="<?=$x1?>" y1="<?=$y1?>" x2="<?=$x2?>" y2="<?=$y2?>" stroke-width="5" stroke="green" />
        <?
    }
    
    function foot(){
            echo '</svg>';
    }
}    



class cmdMaze {
    
    var $larg = 25;
    
    function head(){
    }
    
    function line($x,$y,$sens) {
        if ($sens == 0) {
            print "top:$x:$y,";
        } else {
            print "right:$x:$y,";
        }
    }
    function foot(){
    }
}    


class maze {
    var $maxM = 10;
    var $maxN = 10;
    var $maxA;


    var $cells = array();
    var $bot = array();
    var $right = array();
    var $maze;

    function toA($n,$m) {
        return $n*$this->maxM + $m;
    }

    function toNM($a) {
        $m = $a % $this->maxM;
        $n = floor($a / $this->maxM);
        return array($n,$m);
    }

    function getWall() {
        $cur=0;
        $nei=0;
        $a = 0;
        $sens=0;
        while ($cur == $nei) {
            $sens = rand(0,1);
            $a = rand(0,$this->maxA-1);
            list($n,$m) = $this->toNM($a);
            if ($n == $this->maxN -1 ) $sens=1;
            if ($m ==$this->maxM -1)  $sens=0;
            if ($n == $this->maxN -1  and $m==$this->maxM -1 )  continue;
            if ($sens == 0 ) {
                $t = $this->bot;
                $b = $a + $this->maxM;
            } else {
                $t=$this->right;
                $b = $a+1;
            }
            $cur = $this->cells[$a];
            $nei = $this->cells[$b];
        }
        
        if ($sens == 0 ) {
            $this->bot[$a]=1;
        } else {
            $this->right[$a]=1;
        }
        
        for($i=0; $i < $this->maxA; $i++ ){
            if  ($this->cells[$i] == $nei )  $this->cells[$i]=$cur;
        }
    }
    
    
    
 
    
    function main() {
        
        $this->maze->head();
    
        $this->maxA = $this->maxM * $this->maxN;
    
        for ( $a=0; $a < $this->maxA; $a++) {
            array_push($this->cells,$a);
            array_push($this->bot,0);
            array_push($this->right,0);
        }

        for ( $step=0; $step < $this->maxA -1; $step++) {
            $this->getWall();
        }
        
        
        for($n=0; $n < $this->maxN; $n++ ) {
            $this->maze->line(-1,$n,1);
        }
        
                
        for($m=0; $m < $this->maxM; $m++ ) {
            $this->maze->line($m,-1,0);
        }
    

        
        for($n=0; $n < $this->maxN; $n++ ) {
            for($m=0; $m < $this->maxM; $m++ ) {
                $a=$this->toA($n,$m);
                if ($this->right[$a] == 0) $this->maze->line($m,$n,1);
                if ($this->bot[$a] == 0)    $this->maze->line($m,$n,0);
            }
        }
        $this->maze->foot();
    }
}

$maz = new Maze();

$type = gettest('type','cmd');

if ($type== 'svg' ) {
    $maz->maze = new svgMaze();
} else {
    $maz->maze = new cmdMaze();
}

$maz->main();

?>
