function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

function gettest(key,value) {
    if (array_key_exists(key, _GET)) {
        return _GET[key];
    } else {
        return value;
    }
}


class svgMaze {
    
    constructor() {
        this.larg = 25;
    }
    
    head(){
        let h = `<?xml version="1.0" standalone="no"?>
        <!DOCTYPE svg SYSTEM "svg-19991203.dtd" >
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="300" height="300">
        `
        return h
    }
    
    line(x,y,sens) {
        let x1,y1,x2,y2
        if (sens == 0) {
            x1 = x *this.larg;
            y1 = (y+1)*this.larg;
            x2=x1+this.larg;
            y2=y1;
        } else {
            x1 = (x+1)*this.larg;
            y1 = y*this.larg;
            x2=x1;
            y2=y1+this.larg;
        }

        return `<line  x1="${x1}" y1="${y1}" x2="${x2}" y2="${y2}" stroke-width="5" stroke="green" />
        `
        
    }
    
    foot(){
            return '</svg>';
    }
}    



class cmdMaze {
    constructor() {
        this.larg = 25;
    }
    
    head(){
        return ""
    }
    
    line(x,y,sens) {
        if (sens == 0) {
           return `top:${x}:${y},`;
        } else {
            return `right:${x}:${y},`;
        }
    }
    foot(){
        return ""
    }
}    


class maze {

    constructor() {
        this.maxM = 10;
        this.maxN = 10;
        this.maxA;
        this.cells = [];
        this.bot = [];
        this.right =[];
        this.maze = null;
    }


    toA(n,m) {
        return n*this.maxM + m;
    }

    toNM(a) {
        let m = a % this.maxM;
        let n = Math.floor(a / this.maxM);
        return [n,m];
    }

    getWall() {
        let cur=0;
        let nei=0;
        let a = 0;
        let sens=0;
        while (cur == nei) {
            let n,m,t,b
            sens = getRandomInt(2);
            a    = getRandomInt(this.maxA);
            [n,m] = this.toNM(a);
            if (n == this.maxN -1 ) sens=1;
            if (m ==this.maxM -1)  sens=0;
            if (n == this.maxN -1  && m==this.maxM -1 )  continue;
            if (sens == 0 ) {
                t = this.bot;
                b = a + this.maxM;
            } else {
                t=this.right;
                b = a+1;
            }
            cur = this.cells[a];
            nei = this.cells[b];
        }
        
        if (sens == 0 ) {
            this.bot[a]=1;
        } else {
            this.right[a]=1;
        }
        
        for(let i=0; i < this.maxA; i++ ){
            if  (this.cells[i] == nei )  this.cells[i]=cur;
        }
    }
    
    
    
 
    
    main() {

        let result= this.maze.head();
    
        this.maxA = this.maxM * this.maxN;
    
        for ( let a=0; a < this.maxA; a++) {
            this.cells.push(a);
            this.bot.push(0);
            this.right.push(0);
        }

        for ( let step=0; step < this.maxA -1; step++) {
            this.getWall();
        }
        
        
        for(let n=0; n < this.maxN; n++ ) {
            result += this.maze.line(-1,n,1);
        }
        
                
        for(let m=0; m < this.maxM; m++ ) {
            result += this.maze.line(m,-1,0);
        }
    

        
        for(let n=0; n < this.maxN; n++ ) {
            for(let m=0; m < this.maxM; m++ ) {
                let a=this.toA(n,m);
                if (this.right[a] == 0) result +=this.maze.line(m,n,1);
                if (this.bot[a] == 0)    result +=this.maze.line(m,n,0);
            }
        }
        result += this.maze.foot();

        return result
    }
}


exports.execute= function(type) {
    maz = new maze();

    if (type== 'svg' ) {
        maz.maze = new svgMaze();
    } else {
        maz.maze = new cmdMaze();
    }

    return maz.main();
}



