
string bloc=",@tploc=n|@tplm=n|@tplure=n|@edit=n|@rez=n|@fly=n|@fartouch=n";
reset()
{
    // Coordonnées du prim de destination
    
    llSitTarget(<1.5,0.0,1.0>, ZERO_ROTATION);
    llSetSitText("Teleport");
}

default
{
    state_entry()
    {
        reset();
    }
    
    on_rez(integer startup_param)
    {
        reset();
    }
    
    changed(integer change)
    {
        
        llRegionSay(-134, "Laby,"+(string)llAvatarOnSitTarget()+bloc); 
        
        llUnSit(llAvatarOnSitTarget());
                

        
        reset();
    }
}
