// This Script is distributed under the terms of the modified BSD license, reproduced at the end of
// the script. The license and acknowledgments listed must be included in some fashion if this script
// is redistributed in a non-readable or non-modifiable form.

// Cyberflight 1.1 (gentle flight boost)

float last_alt;
float last_time;
float last_move;
float boost;
integer controls;

float MIN_SPEED = 2.0;
float WANT_SPEED = 16.0;
float MAX_SPEED = 25.0;
float MIN_TIME = 1.0;
float DEFAULT_BOOST = 0.5;

float FAST_TICK = 0.1;
float SLOW_TICK = 1.0;
float LONG_TIME = 5.0; // reset boost if no work in a long time 

float MIN_BOOST_HEIGHT = 72.0;
float MIN_BOOST_CLEARANCE = 36.0;

integer flying = -1;
integer falling = -1;
integer hovering = -1;
integer dimmed = -1;

vector hover_force = <0,0,0>;
float last_hover_check = 0;
float HOVER_CHECK_TIME = 2.0;
float HOVER_MOVEMENT_THRESHOLD = 2.0;
float HOVER_ADJUST_THRESHOLD = 0.01;
float HOVER_ADJUST_FACTOR = 1.05; // 5%

list BOOST_SPEED = [ 16.0, 40.0 ];

integer COMMCHAN = 11337;
list OPTIONS;
list DEFAULT_OPTIONS = [ // default, turn_on, turn_off
    1, "HUD on", "HUD off",
    0, "Lock", "Unlock",
    0, "Hi-boost", "Lo-boost"
];
integer OPTION_STRIDE = 3;
integer OPTION_HUD_ON = 0;
integer OPTION_HUD_LOCK = 1;
integer OPTION_BOOST = 2;
integer listen_handler;
float listen_timeout;

integer hud_on = 1;
integer hud_lock = 0;

list DIRECTIONS = [
    "East","Northeast",
    "North","Northwest",
    "West","Southwest",
    "South","Southeast",
    "E","NE","N","NW","W","SW","S","SE"
];

string last_hud_text;
integer hud_active = 0;
float last_hud_time = 0; 
show_hud()
{
    if(hud_lock)
        hud_active = 1;
    else if(hud_on)
    {
        if(hud_active >= 0 && flying == 0 && hovering == 0 && falling == 0) hud_active = 0;
        else if(hud_active <= 0 && (flying > 0 || hovering > 0 || falling > 0)) hud_active = 1;
    }
    else if(hud_active == 1)
        hud_active = 0;

    if(hud_active == 1) {
        float time = llGetTime();
        if(last_hud_time > time - 1) return;
        vector v = llGetVel();
        vector p = llGetPos();
        string boost_text = "";
        if(hovering)
        {
            if(hover_force.z > 9.81)
                boost_text = "\nHover+";
            else if(hover_force.z < 9.79)
                boost_text = "\nHover-";
            else if(controls == 0)
                boost_text = "\nHover";
        }
        if(falling)
            boost_text = "\nInactive";
        if(boost > 0 && controls > 0)
            boost_text = "\nBoost "+((string)llFloor(boost * 100.0 / 6.0 + 0.5))+"%";
        
        string direction = "";
        float hs = llVecMag(<v.x,v.y,0>);
        float vs = llFabs(v.z);
        if(hs > 0.5)
        {
            float theta = llAtan2(v.y, v.x);
            integer zone = (llFloor((theta + PI/8) / (PI/4)) + 8) % 8; 
            if(vs > 1) zone += 8;
            direction += llList2String(DIRECTIONS, zone) + " "
                       + ((string)llFloor(hs+0.5)) + " m/s";
        }
        if(vs > 1)
        {
            integer index = 0;
            if(v.z > 0)
                index = 1;
            if(direction != "")
            {
                index += 2;
                direction += ", ";
            }
            direction += llList2String(["Descent","Ascent","dn","up"],index) + " "
                       + ((string)llFloor(vs+0.5)) + " m/s";
        }
        if(direction != "")
            direction = "\n" + direction;

        string t =
            "<"+((string)llFloor(p.x+0.5))+","+((string)llFloor(p.y+0.5))+","+((string)llFloor(p.z+0.5))+">"+
            " ("+((string)(llFloor(p.z-llGround(<0,0,0>)+0.5)))+"m AGL)"+
            direction+boost_text;
        if(last_hud_text != t) {
            llSetText(t, <1,1,1>, 0.7 + hud_lock * 0.3);
            last_hud_text = t;
            last_hud_time = time;
        }
    } else if(hud_active == 0) {
        last_hud_text = "";
        last_hud_time = 0;
        llSetText("", ZERO_VECTOR, 0);
        hud_active = -1;            
    }
}


set_hover(integer active)
{
    if(active == hovering) return;
    hovering = active;
    if(hovering) hover_force = <0,0,9.8>;
    else hover_force = ZERO_VECTOR;
    llSetForce(hover_force * llGetMass(), FALSE);
    last_hover_check = llGetTime() + 5.0; // extra time to settle down
}


check_hover()
{
    if(!hovering) return;
    float t = llGetTime();
    if(t - last_hover_check < HOVER_CHECK_TIME) return;
    last_hover_check = t;
    vector v = llGetVel();
    if(llVecMag(v) > HOVER_MOVEMENT_THRESHOLD) return;
    if(v.z > HOVER_ADJUST_THRESHOLD) hover_force.z /= HOVER_ADJUST_FACTOR;
    else if(v.z < -HOVER_ADJUST_THRESHOLD) hover_force.z *= HOVER_ADJUST_FACTOR;
    else return;
    llSetForce(hover_force*llGetMass(), FALSE);
}

float last_tick = -1;
set_tick(float tick)
{
    if(tick == last_tick) return;
    last_tick = tick;
    llSetTimerEvent(tick);
}

float last_boost_height;
float average_boost;
check_boost()
{
    flying = 1;
    falling = 0;

    integer info = llGetAgentInfo(llGetOwner());

    if((info & AGENT_FLYING) == 0)
    {
        set_hover(FALSE);
        falling = (info & AGENT_IN_AIR) != 0;
        flying = 0;
        boost = 0;
        set_tick(SLOW_TICK);
        return;
    }

    vector pos = llGetPos();
    if(pos.z < last_boost_height / 2) // trim running average of boost if big altitude drop
        average_boost = average_boost * (pos.z / last_boost_height)
                      + DEFAULT_BOOST * (1.0 - pos.z / last_boost_height);

    if(pos.z < MIN_BOOST_HEIGHT || pos.z - llGround(<0,0,0>) < MIN_BOOST_CLEARANCE)
    {
        set_hover(FALSE);
        set_tick(SLOW_TICK);
        boost = 0;
        return;
    }

    set_hover(TRUE);
    
    if(controls <= 0) return;

    vector vel = llGetVel();
    float time = llGetTime();
    float speed = vel.z;
    float target = WANT_SPEED;
    float window = WANT_SPEED / 16.0;
    
    if(speed > 0)
        last_move = time;

    if(time - last_time >= LONG_TIME)
        boost = 0;
    else
    {
        if(speed < target - window)
        {
            if(boost == 0)
                boost = average_boost;
            if(time - last_move > MIN_TIME) boost += 0.4;
            else if(speed < target * 0.25) boost += 0.2;
            else if(speed < target * 0.5 ) boost += 0.1;
            else if(speed < target * 0.75) boost += 0.05;
            else if(speed < target - window * 4) boost += 0.02;
            else boost += 0.01;
        }
        else if(speed > MAX_SPEED) boost -= 0.5;
        else if(speed > target + window * 4) boost -= 0.1;
        else if(speed > target + window * 2) boost -= 0.03;
        else if(speed > target + window) boost -= 0.01;

        if(boost <= 0)
            boost = 0;
        if(boost > 0) {
            llApplyImpulse(<0,0,boost> * llGetMass(), FALSE);
            average_boost = average_boost * 0.9 + boost * 0.1; // 10 sample running average
            last_boost_height = pos.z;
        }
    }
    
    if(boost) set_tick(FAST_TICK);
    else set_tick(SLOW_TICK);
    last_alt = pos.z;
    last_time = time;
}

take_controls()
{
    llTakeControls(CONTROL_UP|CONTROL_DOWN,TRUE,TRUE);
}

request_perms()
{
    if(llGetPermissions() & PERMISSION_TAKE_CONTROLS)
        take_controls();
    else
        llRequestPermissions(llGetOwner(), PERMISSION_TAKE_CONTROLS);
}

request_controls()
{
    if(llGetAttached()) request_perms();
}

post_dialog()
{
    listener(TRUE);
    listen_timeout = llGetTime() + 30;
    list buttons = [];
    integer n = llGetListLength(OPTIONS);
    integer i;
    for(i = 0; i < n; i += OPTION_STRIDE)
    {
        if(llList2Integer(OPTIONS, i))
            buttons += llList2String(OPTIONS, i+2);
        else
            buttons += llList2String(OPTIONS, i+1);
    }
    llDialog(llGetOwner(), "Cyberflight options", buttons, COMMCHAN);
}

check_dialog()
{
    if(listen_handler)
    {
        if(llGetTime() > listen_timeout)
        {
            listener(FALSE);
        }
    }
}

listener(integer onoff)
{
    if(listen_handler) llListenRemove(listen_handler);
    if(onoff)
        listen_handler = llListen(COMMCHAN, "", llGetOwner(), "");
    else
        listen_handler = 0;
}

handle_command(string message)
{
    listener(FALSE);
    integer i = llListFindList(OPTIONS, [message]);
    if(i == -1) return;
    integer option = i / OPTION_STRIDE;
    integer opindex = option * OPTION_STRIDE;
    integer onoff = (i % OPTION_STRIDE) == 1;
    OPTIONS = llListReplaceList(OPTIONS, [onoff], opindex, opindex);
    if(option == OPTION_HUD_ON)
        hud_on = onoff;
    else if(option == OPTION_HUD_LOCK)
        hud_lock = onoff;
    else if(option == OPTION_BOOST)
        WANT_SPEED = llList2Float(BOOST_SPEED, onoff);
}

init()
{
    boost = 0;
    
    flying = -1;
    falling = -1;
    hovering = -1;
    dimmed = -1;
    hud_on = 1;
    hud_lock = 0;
    WANT_SPEED = 16;
    OPTIONS = DEFAULT_OPTIONS;

    set_tick(SLOW_TICK);
    request_controls();
}


default
{
    state_entry()
    {
        init();
    }
    
    on_rez(integer param)
    {
        init();
    }
    
    changed(integer what)
    {
        if(what & CHANGED_LINK && llGetAttached()) init();
    }

    run_time_permissions(integer mask)
    {
        if(mask & PERMISSION_TAKE_CONTROLS) take_controls();
    }
    
    control(key id, integer level, integer edge)
    {
        controls = 0;
        if(level & CONTROL_UP) controls++;
        if(level & CONTROL_DOWN) controls--;
        check_boost();
    }
    
    timer()
    {
        check_boost();
        check_hover();
        show_hud();
        check_dialog();
        if(flying && boost > 0)
        {
            if(dimmed != 0)
                llSetAlpha(1.0,ALL_SIDES);
            dimmed = 0;
        }
        else
        {
            integer dimness = 4 + hud_on + hud_lock * 2;
    
            if(dimmed != dimness) {
                float alpha = ((float)dimness) / 10;
                llSetAlpha(alpha,ALL_SIDES);
            }
            dimmed = dimness;
        }
    }
    
    listen(integer chan, string name, key id, string message)
    {
        handle_command(message);
    }
    
    touch_start(integer num)
    {
        if(llDetectedKey(0) == llGetOwner())
        {
            post_dialog();
        }
    }
}


//Copyright (c) 2005,2006, Argent Stonecutter & player
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//    * Redistributions in modifiable form must retain the above copyright notice, this list of conditions and the following disclaimer.
//    * Redistributions in non-modifiable form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//    * Neither the name of Argent Stonecutter nor his player may be used to endorse or promote products derived from this software without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

