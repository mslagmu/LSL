integer canal;
key keykeeper = NULL_KEY;
OwnerSay(string s) {
    //if (llGetOwner()  == "44a74377-0fa3-4c39-8b9e-0956be6e5f9a" ) return;
     llOwnerSay(s); 
}
  
default
{
    on_rez(integer nb) {
        if ( keykeeper != NULL_KEY ) {
            OwnerSay ("@detach=n");
        };
    }
    
    state_entry()
    {
        canal = -((integer) llFrand (1000000.0)+10005);
        llListen(canal,"","","");
    }
    
    touch_start(integer nb) {
        key av = llDetectedKey(0);
        if ( keykeeper == NULL_KEY) {
            llDialog(av,"Que voulez vous faire ?", ["Prendre clef"],canal);
            return;
        }
        if ( keykeeper == av) {
            llDialog(av,"Que voulez vous faire ?", ["Rendre clef"],canal);
        } else {
            llSay(0,"Pas touche !!!!! Clé prise par " + llKey2Name(keykeeper));    
        }
    }
    
    listen(integer channel, string name, key id, string message) 
    {
        if ( message == "Prendre clef" ) {
            llSay(0,"Clé prise par " + llKey2Name(id));
            keykeeper = id;
            OwnerSay ("@detach=n");   
        }
        
        if ( message == "Rendre clef" ) {
            llSay(0,"Clé rendue par " + llKey2Name(id));
            keykeeper = NULL_KEY; 
            OwnerSay ("@detach=y");     
        }
    }
}


 
