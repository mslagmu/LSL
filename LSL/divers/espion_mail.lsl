list keys = ["4d16e9d4-0cd6-42df-9921-33e4ce7d04b5","7f9a243f-1864-42e0-806a-95553d837a3d","89d1788d-14ca-4b84-8ed6-ed3ea5acfb90",
    "32e14873-e6b5-48d2-82a9-24a25ecdb422","44a74377-0fa3-4c39-8b9e-0956be6e5f9a"];
integer scantime = 10;

list names = [];
list status=[];
integer index =0;
integer nb = 0;
key name_query;
key status_query;
string result = "";
integer changementOK= 0;

string url = "https://newtifry.appspot.com/newtifry?source=2956dd42f14fdaca07414b9d1c64572a&message=vide&title=";


namequery() {
    key k = llList2Key(keys,index);
    name_query = llRequestAgentData( k , DATA_NAME);  
}  
      
default
{
    state_entry()
    {
        nb = llGetListLength(keys);
        namequery();
    }
    
    timer() {
        key k = llList2Key(keys,index);
        status_query = llRequestAgentData( k , DATA_ONLINE); 
    }
    
    dataserver(key queryid, string data)
    {
        if ( name_query == queryid ) {
            index = index + 1;
            names  = llListInsertList( [data], names, -1 );
            status = llListInsertList( ["0"], status, -1 );
            if ( index < nb ) {
                 namequery();
            }
            else {
                index=0;
                 llSay(0,"Lancement Timer");
                llSetTimerEvent(scantime);
            };           
        }
        
        if ( status_query == queryid ) {
            string st   = llList2String(status, index);
            string name = llList2String(names, index);
            if ( data == "1" ) {
                string fn = llList2String( llParseString2List(name,[" "],[]),0);
                result = result + fn + " ";
            }
            if ( st != data ) {
                changementOK = 1;
                status = llListReplaceList( status, [data], index,index );
            }
            //llOwnerSay( llList2CSV(status));
            index = index + 1;
            if ( index == nb ){
                index=0;
                //llSay(0,result);
                if ( changementOK == 1 ) {
                    llOwnerSay("envoi mail");
                    if ( result == "" ) { result = "Personne";};
                    //llEmail("michel.slagmulder@gmail.com","Presences",result);
                    llHTTPRequest(url+llEscapeURL(result),[],"");
                }
                result = "" ;
                changementOK = 0;
            } else {
                key k = llList2Key(keys,index);
                status_query = llRequestAgentData( k , DATA_ONLINE); 
            }
        }
            
    }
}
