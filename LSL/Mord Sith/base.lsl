string owners="";
string captives="";
integer channel=-4589632;
integer collarchannel=-4580632;
key add_request;
key load_request;
string type = "M";
string role;

string url = "http://douceamande.alwaysdata.net/ms.php";

notif(string msg) {
    llSay(0,msg);
}

sendchannel(string msg) {
     llRegionSay(collarchannel,msg);
}



toCollars() {

     string mesg = "owner=" + owners;
      
    sendchannel(mesg);
}


charger () {
    load_request= llHTTPRequest(url+"?action=list", [], "");   
} 


http_handle(key ID, string body) {
    list l = llParseString2List(body,["|"],[]);
    string cmd = llList2String(l,0);
    string data= llList2String(l,1);
    
    if (cmd=="list") {
        llHTTPResponse(ID, 200, owners);
    }
    
    if (cmd="issub") {
          integer pos =  llSubStringIndex(captives,data);
          if (pos==-1) {
               llHTTPResponse(ID, 200, "issub|KO");
          } else {
              llHTTPResponse(ID, 200, "issub|OK");
         }
    }
    if (cmd == "listserver") {
        llSay(0,"coucou:" + data);    
    }
}


default
{
    state_entry()
    {
        llListen(channel,"", "", "");
        llRequestURL();
        charger();
    }

    touch_start(integer total_number)
    {
        llDialog(llDetectedKey(0),"Choisir",["Ajout","Envoyer","Charger","URL"],channel);  
    }
    
    listen( integer canal, string name, key id, string message )
    {
        if ( message == "Ajout" ) {
            llDialog(id,"Choisir",["Captive","Maitresse"],channel);
            return;
        }
        
        if ( message == "Captive" ) {
            role = message;
            type="S";
            state newMistress;
            return;
        }
        
        
        if ( message == "Maitresse" ) {
            role = message;
            type="M";
            state newMistress;
            return;
        }
        
        if ( message == "URL" ) {
            llInstantMessage(id,url);
            return;
        }
        
        if ( message == "Charger" ) {
            charger();
            return;
        }
        
        if ( message == "Envoyer" ) {
            toCollars();
            return;
        }
    }
    
    http_response(key request_id, integer status, list metadata, string body)
    {
        if (request_id == load_request)
        {
            list l = llParseString2List(body,["\n"],[]);
            owners = llList2String(l,0);
            captives= llList2String(l,1);
            toCollars();
        }
    }
    
     http_request(key ID, string Method, string Body) {
        if (Method == URL_REQUEST_GRANTED) {
            //Saying URL to owner
           llHTTPRequest(url+"?action=sendurl&url="+Body, [], "");
        } else {
            http_handle(ID,Body);
        }
    }
    
     changed(integer What) {
        //Region restarted
        if (What & CHANGED_REGION_START) {
            //Request new URL
            llRequestURL();
        }
    
    }
}


state newMistress
{
    state_entry()
    {
        notif("Le systeme est pret à enregister une nouvelle "+ role +" MS");
    }
    
    touch_start(integer total_number)
    {
        key av = llDetectedKey(0);
        string name = llKey2Name(av);
        notif(name + " est enregistrée comme nouvelle " + role );
        add_request= llHTTPRequest(url+"?action=add&type="+type+"&key="+(string)av+"&name="+llEscapeURL(name), [], "");
        state default;
    }
    
    
}
