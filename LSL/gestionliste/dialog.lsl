list listitem=[];
string message ="";
integer nbitems;
integer buttom=0;


integer dlgchannel;
key avatar ;
integer channel=100;

dialog(integer buttom) {
    integer top;
    top = buttom + 10;
    list shown = llList2List(listitem,buttom,top-1); 
    llDialog(avatar,message,["<",">"]+shown,channel);
}


default
{
    state_entry()
    {
        channel = -(1000+(integer)llFrand(10000));
        llListen(channel, "", "", "");

    }
    

    touch_start(integer total_number)
    {
       /* llMessageLinked(LINK_THIS, 0, "Test Test", "");
        llMessageLinked(LINK_THIS, 1, "1|2|3|4|5|6|7|8|9|10|11|12|14|15|16", llDetectedKey(0));*/
    }
    

    
    listen( integer canal, string name, key id, string message )
    {
        
        if ( message == "<" ) {
            if ( buttom >= 10 ) buttom = buttom-10;
            dialog(buttom);
            return;
        }
        
        
        if ( message == ">" ) {
            if ( buttom + 10 < nbitems ) buttom = buttom+10;
            dialog(buttom);
            return  ;
        }
        
        
        llMessageLinked(LINK_THIS, 3, message, avatar);
        
    }
    
    
    
    link_message(integer sender_num, integer num, string arg, key id) {
        integer offset;
        if (num == 0 ) {
              message = arg;
              return ;
        }
        if (num == 1) {
            avatar = id;
            listitem = llParseString2List(arg, [";"], []);
            nbitems = llGetListLength(listitem);
            dialog(0);
        }
        
        if (num == 2) {
            avatar = id;
            llTextBox( avatar, message, channel );
        }
    }    
    
}
