string avatars="";
integer radarChannel=-18547965;
float xmin = 4;
float ymin = 136;
float xmax = 40;
float ymax = 184; 
integer isactived = 0;

key NCQuery;
integer iLine = 0;




default
{
    state_entry()
    {
        llListen(radarChannel, "", NULL_KEY ,"" );
        llMessageLinked(LINK_ALL_OTHERS, 5, "OFF", "");
    }

    link_message(integer sender_num, integer num, string str, key id)
    {

        if (num == 4) {
            avatars = str;
        }
        
        if (num == 5) {
            
            if ( str == "ON" ) {
                llSensorRepeat("", "", AGENT, 96.0, PI, 15.0);
            }
            
            if ( str == "OFF" ) {
                llSensorRemove( );
            }
        }               
    }

     sensor(integer number_detected)
    {
        integer i;
        for(i=0; i < number_detected;i++) {
            vector pos = llDetectedPos(i);
            key av = llDetectedKey(i);  
            string name =   llDetectedName(i);        
            if ( av!="0294af58-9b7e-470e-8939-4505c98d9c13" && av!="44a74377-0fa3-4c39-8b9e-0956be6e5f9a") {;
                if ( (pos.x > xmin && pos.x < xmax && pos.y > ymin && pos.y < ymax ) && (llSubStringIndex(avatars,name) ==-1 ) ){
                    llInstantMessage(llDetectedKey(i), "ALERTE !!!! Vous n'avez pas à vous trouver là!!!");
                    llEjectFromLand(llDetectedKey(i));
                }
            }
        }

    }
    

    
}
