string permis="";
string demande="";
integer pupitrechannel;
string action;


string delete(string source, string name) {
    integer index = llSubStringIndex(source,name);
    integer l = llStringLength(name);
    if ( index != -1 ) {
        source = llDeleteSubString(source,index,index+l);
    }
    

    return source;
}


inscription(string name) {
    if (llSubStringIndex(demande,name)!=-1) {
        llSay(0,"Vous avez une demande d'inscription en cours");
        return;
    }
    
    if (llSubStringIndex(permis,name)!=-1) {
        llSay(0,"Vous êtes déjà inscrit(e)");
        return;
    }
    demande=demande+name+";";
}


export() {
    
    llSay(0,permis);  
}

validation(key toucher){
    llMessageLinked(LINK_THIS, 0, "Liste des demandes", "");
    llMessageLinked(LINK_THIS, 1, demande+"effacer",toucher);   
}

ejection(key toucher){
    llMessageLinked(LINK_THIS, 0, "Qui voulez vous suprimer ?", "");
    llMessageLinked(LINK_THIS, 1, permis,toucher);   
}


reinit(key toucher) {
    llMessageLinked(LINK_THIS, 0, "Entrez la site de l'export", "");
    llMessageLinked(LINK_THIS, 2, "",toucher);
}

rafraichir(){
    llMessageLinked(LINK_SET, 4, permis, "");
}

default
{

    state_entry(){
        pupitrechannel = -(1000+(integer)llFrand(10000));
        llListen(pupitrechannel, "", "", "");        
    }
    

     
    
    link_message(integer sender_num, integer num, string str, key id)
    {
        if (num == 3) {
            if (action == "validation") {
                if ( str == "effacer" ) {
                    demande = "";
                    return;    
                };
                
                demande = delete(demande,str);
                permis = permis + str + ";";
                llSay(0, str + " a été validé(e) par " + llKey2Name(id));
                validation(id);
                rafraichir();
            }
            if (action == "ejection") {
                permis = delete(permis,str);
                llSay(0, str + " a été ejectée(e) par " + llKey2Name(id));
                rafraichir();
            }
            if (action == "reinit") {
                permis = str;
                llSay(0, "La liste  a été reinitailisée par " + llKey2Name(id));
                rafraichir();
            }
            if (action == "desincription") {
                string name = llKey2Name(id);
                llSay(0,name+" est désinscrit(e).");
                demande = delete(demande,name);
                permis = delete(permis,name);
                rafraichir();
            }
            
        }
    }
    
    
    touch_start(integer total_number)
    {
        vector pos    = (llDetectedTouchPos(0) - llGetPos()) / llGetRot();
        vector scale  = llGetScale();
        string message;
        pos.x=pos.x/scale.x+0.5;
        pos.y=pos.y/scale.y+0.5;
        pos.z=pos.z/scale.z+0.5;
        
        float x=pos.y;
        float y=1-pos.x;
                
        string name = llDetectedName(0);
        key av = llDetectedKey(0);
        if ( av=="0294af58-9b7e-470e-8939-4505c98d9c13" ||  av=="44a74377-0fa3-4c39-8b9e-0956be6e5f9a") {
            if ( y<0.72 && y>0.58 && x<0.24 ) {
                action = "validation";
                validation(av);
            }
            
            if ( y<0.54 && y>0.40 && x<0.24 ) {
                action = "ejection";
                ejection(av);
            }
            
            if ( y<0.27 && y>0.13 && x<0.24 ) {
                llMessageLinked(LINK_SET, 5, "ON", "");
            }
            
            if ( y<0.72 && y>0.58 && x>0.28 && x<0.52 ) {
                export();
            }
            
            if ( y<0.54 && y>0.40 && x>0.28 && x<0.52  ) {
                action = "reinit";
                rafraichir();
                reinit(av);
            }
            
            if ( y<0.27 && y>0.13  && x>0.28 && x<0.52  ) {
               llMessageLinked(LINK_SET, 5, "OFF", "");
            }
        }
        if ( y<0.70 && y>0.44  && x>0.60   ) {
            llSay(0,name+" a demandé son inscription.");
            inscription(name);
        }
        
        if ( y<0.36 && y>0.11  && x>0.60   ) {
            if (llSubStringIndex(demande+permis,name)!=-1) {
                llMessageLinked(LINK_THIS, 0, "Voulez vous vraiment vous désincrire", "");
                llMessageLinked(LINK_THIS, 1, "OUI",av);
                action = "desincription";
            } else {
                llSay(0,name + " n'est pas inscrit(e) dans le système");
            }
        }
        
        //llSay(0,"demande: "+demande);
        //llSay(0,"permis: "+permis);
        
        //llSay(0,(string)x +":" + (string)y+ " : " + message);
    }
}
