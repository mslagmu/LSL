//========================================
// HOW TO INSTALL
// put this script in lamp and the other in the switch
// fill the description of the lamp with the name of the switch
// Is done !! Enjoy !!!
//======================================================

integer ison = 0;
integer channel = -16521489;

switchon () {
     llSetPrimitiveParams( [PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
                            PRIM_GLOW,ALL_SIDES,1.0,
                            PRIM_POINT_LIGHT,TRUE,<1.0, 1.0, 1.0>,1.0,10.0,1.0
                            ] );
}


switchoff () {
     llSetPrimitiveParams( [PRIM_FULLBRIGHT,ALL_SIDES,FALSE,
                            PRIM_GLOW,ALL_SIDES,0.0,
                            PRIM_POINT_LIGHT,FALSE,<1.0, 1.0, 1.0>,1.0,10.0,1.0
                        ] );
}

default
{
    state_entry()
    {
        llListen(channel, "", "", "");
    }

    listen( integer canal, string name, key id, string message )
    {
        if ( name != llGetObjectDesc() ) return;
        if ( message== "ON")  switchon();
        if ( message== "OFF") switchoff();
    }

}

