integer channel = -5555;
integer rlvCan =-1812221819;
list lav;

string bloc  =",@tploc=n|@tplm=n|@tplure=n|@sendim=n|@recvim=n|@edit=n|@rez=n|@fly=n|@fartouch=n|@chatnormal=n|@showminimap=n|@showworldmap=n";
string debloc=",@tploc=y|@tplm=y|@tplure=y|@sendim=y|@recvim=y|@edit=y|@rez=y|@fly=y|@fartouch=y|@chatnormal=y|@showminimap=y|@showworldmap=y";

//string bloc=",@fly=n|@fartouch=n";
//string debloc=",@fly=y|@fartouch=y";

incarceration( key id ) {

    if ( llListFindList( lav, [id] ) == -1 ){
        llSay(0,llKey2Name(id) + " is captured." );
        llRegionSay(rlvCan, "Prison,"+(string)id+bloc);
        lav = lav + [id];
    }
}


liberation( ) {
   integer  nb; 
   integer i;
   key id;
   nb = llGetListLength(lav);
   
   for(i=0; i < nb ; i++ ) {
       id = llList2Key(lav,i);
       llSay(0,llKey2Name(id) + " was freed" );
       llRegionSay(rlvCan, "Prison,"+(string)id+debloc);
   }
   
   lav = [];
   
}

default
{
    
    state_entry()
    {   
        llListen(channel, "", NULL_KEY, "");
        llListen(rlvCan, "","", "");
    }


    listen( integer canal, string name, key id, string message )
    {
        if ( canal == channel ) {
            if ( message == "p" ) {
                 llSensor( "", NULL_KEY, AGENT, 96.0, PI );
            } else {
                liberation( );
            }
        }
        
        
        if ( canal = rlvCan ) {
                list l = llCSV2List(message);
          
            if ( llList2String(l,0) == "ping" ) { 
                key av = llGetOwnerKey(id);
                 if ( llListFindList( lav, [av] ) > -1 ) {
                    llRegionSay(canal,"ping,"+(string)llGetOwnerKey(id)+",!pong");
                 }
                }        
        }
  
    }


    touch_start(integer total_number)
    {
       //llSensor( "", NULL_KEY, AGENT, 96.0, PI );
        
    }
    
    sensor (integer numberDetected)
    {
        key id;
        integer i;
        vector pa;
        vector po;
        vector so;


        po = llGetPos();
        so = llGetScale()/2;

        for (i = 0; i < numberDetected; i++)
        {
            id = llDetectedKey(i);
            pa = llDetectedPos(i) - po;
            
            if ( (llFabs(pa.x) <= so.x ) && (llFabs(pa.y) <= so.y ) ) {     
                incarceration( id );
            }
        }

    }
    
    no_sensor() {
        llSay(0,"Personne");    
    }
    
}
