vector mov=<0.0,-2.0,0.0>;
integer isopen=1;
key avkey = NULL_KEY;
integer canal = -489654;
integer zoncan = -5555;
integer time = 0;
integer leftTime = 0;
string avname;

list ltimes = ["0","15min","30min","1h"," "," "];


setTime () {
    string t;
    string k;
    vector c;
    if ( leftTime == 0 ) {
        t = "*****";
        c = <0.0, 1.0, 0.0>;
    } else {
        t = getTimeText(leftTime);
        c = <1.0, 0.0, 0.0>;
    };
    
    if ( avkey == NULL_KEY ) {
        k = "Rendu";
    } else {
        k = avname;
    }
    
    
    
    
    llSetText("Temps Restant: " + t + " Clé:" + k, c, 1.0);
}

action() {
    
        if ( isopen== 0 ) {
            llSetPos(llGetLocalPos() + mov);
            llSay(zoncan,"");
            isopen=1;
        } else {
            llSetPos(llGetLocalPos() - mov);
            isopen=0;
            llSay(zoncan,"p");
        }
        manageTimer();
}


string getTimeText(integer time) {
    integer h = time / 3600;
    integer m = (time % 3600)/60;
    return (string)h + "h" + (string)m + "min" ; 
}




manageTimer() {
    if (isopen == 0  && time >0) {
        leftTime = time;
        llSetTimerEvent(60);
    } else {
        llSetTimerEvent(0);
        leftTime=0;
    }
    setTime ();
}

integer dialogs(key av) {
        string    timeText; 
        
        if ( time == 0 )
            {
                timeText = "Inactive";
            }  else {
                timeText = (string) getTimeText(time);
            }         
        if ( avkey == NULL_KEY ) {
      
            llDialog( av, "Action\nTime = " + timeText, ltimes + [ "Prendre la clé","Actionner Porte"], canal );
            return 1;
        }
         
        if ( av == avkey || av == llGetOwner() ) {
            llDialog( av, "Action\nTime = " + timeText, ltimes + ["Rendre la clé","Actionner Porte"], canal );
            return 1;
        }
        
        return 0;
}


default
{
    state_entry()
    {
        llListen(canal,"","","");
        setTime();
    }

    touch_start(integer total_number)
    {

        key av=llDetectedKey(0);
        if ( dialogs(av) == 0) { 
            llSay(0,"Vous ne pouvez agir sur cette porte");
        }
    }
    
    
    listen( integer canal, string name, key id, string message ) {
        string name = llKey2Name(id);
        if (message == "Prendre la clé") {
            avkey=id;
            avname = name;
            setTime ();
            llSay(0, name + " a pris la clé");
            dialogs(id);
            return;
        }
        
        if (message == "Rendre la clé") {
            avkey=NULL_KEY;
            avname = "";
            setTime ();
            llSay(0, name + " a rendu la clé");
            dialogs(id);
            return;
        }
        
        if (message == "Actionner Porte") {
            action();
            dialogs(id);
            manageTimer();
            return;
        }
       
       
        if (message == "0") {
            time = 0;
            dialogs(id);
            manageTimer();
            return;
        }
       
       
        if (message == "15min") {
            time = time + 900;
            dialogs(id);
            manageTimer();
            return;
        }
        
        if (message == "30min") {
            time = time + 1800;
            dialogs(id);
            manageTimer();
            return;
        }
        
        
     
        if (message == "1h") {
            time = time + 3600;
            dialogs(id);
            manageTimer();
            return;
        } 
        
        if (message == "5s") {
            time = time + 60;
            dialogs(id);
            llSetTimerEvent(10);
            return;
        } 
       
    }
    
    timer()
    {
        leftTime = leftTime - 60;
        setTime ();
        if ( leftTime <= 0 ) {
            llSay(0,"Porte Débloquée");
            action();
        }
    } 
    
}
