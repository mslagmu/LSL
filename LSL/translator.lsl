string ownerlng = "fr";
string otherlng = "en";
key ownertrans = NULL_KEY;
key othertrans = NULL_KEY;
integer active = 1;
integer chat = 0;


string getSentence(string s ) {
    list l = llParseString2List(s,["\""],[]);
    return llList2String(l,1);   
}

settext() {
     llSetText( otherlng + " : " + (string)active, <0.0,1.0,0.0>,1.0);   
}

default
{
    state_entry()
    {
        chat = llListen(0,"","","");
        llListen(-5000,"","","");
        settext();
    }

    touch_start(integer total_number)
    {
        llDialog( llGetOwner(), "choisir votre langue", ["en","de","es","it","ru","active"], -5000 );
    }
    
    
    listen( integer canal, string name, key id, string message )
    {
        if (canal == -5000) {
            
            if ( message == "active" ) {
                if (active == 1 ) {
                    active = 0;
                    llListenRemove(chat);
                } else {
                    active = 1;
                    chat = llListen(0,"","","");
                }
                settext();
                return; 
            }
            
            otherlng = message;
            settext();
            return;
        }
        
        if (canal == 0 && id !=  llGetOwner()) {
            othertrans = llHTTPRequest("https://translate.googleapis.com/translate_a/single?client=gtx&sl="+otherlng+"&tl="+ownerlng+"&dt=t&q="+llEscapeURL(message), [],"");
        }
        if (canal == 0 && id ==  llGetOwner()) {
             ownertrans = llHTTPRequest("https://translate.googleapis.com/translate_a/single?client=gtx&sl="+ownerlng+"&tl="+otherlng+"&dt=t&q="+llEscapeURL(message), [],"");
        }   
    }
    
    http_response(key id, integer status, list metadata, string body){
        if (id == ownertrans) {
            llSay(0,ownerlng + ">" + otherlng + " : " + getSentence(body));  
        }
        
        if (id == othertrans) {
            llOwnerSay(otherlng + ">" + ownerlng + " : " + getSentence(body));  
        }
        
    }
}
 
